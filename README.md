1. `git clone *this repo*`
2. `apt install virtualenv docker python3-pip libssl-dev`
3. `/usr/bin/virtualenv -p /usr/bin/python3 .venv`
4. `pip install -r requirements.txt`

##### Init new ansible role with molecule scenario
`molecule init role -r myFirstRole`

##### Init new molecule scenario inside existing ansible role
`molecule init scenario -r myFirstScenario`

##### Init new ansible role with molecule scenario using vagrant and libvirt
`molecule init role -r myFirstRole -d vagrant`

#### Todo
setup docker driver
setup vagrant + libvirt driver
